const path = require('path');

module.exports = {
    lintOnSave: true,
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            title: 'Разработка порталов comdev.ru'
        }
    },

    configureWebpack: {
        resolve: {
            alias: {
                '@api': path.resolve(__dirname, './src/api'),
                '@components': path.resolve(__dirname, './src/components'),
                '@domain': path.resolve(__dirname, './src/domain'),
                '@mixins': path.resolve(__dirname, './src/mixins'),
                '@common': path.resolve(__dirname, './src/common'),
                '@images': path.resolve(__dirname, './src/assets/images'),
                '@fonts': path.resolve(__dirname, './src/assets/fonts'),
                '@router': path.resolve(__dirname, './src/router'),
                '@store': path.resolve(__dirname, './src/store'),
                '@utils': path.resolve(__dirname, './src/utils'),
                '@views': path.resolve(__dirname, './src/views'),
                '@sass': path.resolve(__dirname, './src/sass')
            }
        }
    }
};
